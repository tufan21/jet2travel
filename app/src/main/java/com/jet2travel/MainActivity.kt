package com.jet2travel

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jet2travel.injection.ViewModelFactory
import com.jet2travel.ui.article.ArticleListViewModel
import com.jet2travel.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
  private lateinit var binding: ActivityMainBinding
  private lateinit var viewModel: ArticleListViewModel
  private var errorSnackbar: Snackbar? = null

  override fun onCreate(savedInstanceState: Bundle?){
    super.onCreate(savedInstanceState)

    binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    binding.articleRecyclerList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    binding.articleRecyclerList.apply {
      addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(ArticleListViewModel::class.java)
    viewModel.errorMessage.observe(this, Observer {
        errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
    })
    binding.viewModel = viewModel
  }

  private fun showError(@StringRes errorMessage:Int){
    errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
    errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
    errorSnackbar?.show()
  }

  private fun hideError(){
    errorSnackbar?.dismiss()
  }
}
