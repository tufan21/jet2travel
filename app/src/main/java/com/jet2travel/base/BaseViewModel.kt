package com.jet2travel.base

import androidx.lifecycle.ViewModel
import com.jet2travel.injection.component.DaggerViewModelInjector
import com.jet2travel.injection.component.ViewModelInjector
import com.jet2travel.injection.module.NetworkModule
import com.jet2travel.ui.article.ArticleListViewModel
import com.jet2travel.ui.article.ArticleViewModel

/**
 * Created by tufan on 30,April,2020
 */
abstract class BaseViewModel: ViewModel(){
    //...
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is ArticleListViewModel -> injector.inject(this)
            is ArticleViewModel -> injector.inject(this)
        }
    }
}