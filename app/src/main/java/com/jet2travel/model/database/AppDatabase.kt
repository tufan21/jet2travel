package com.jet2travel.model.database

/**
 * Created by tufan on 30,April,2020
 */
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jet2travel.model.Article
import com.jet2travel.model.ArticleDao
import com.jet2travel.model.DataRoomConverter

@Database(entities = [Article::class], version = 1)
@TypeConverters(DataRoomConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articleDao(): ArticleDao
}