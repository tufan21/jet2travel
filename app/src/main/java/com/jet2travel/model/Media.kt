package com.jet2travel.model

import androidx.room.Entity

/**
 * Created by tufan on 30,April,2020
 */

data class Media (
    val id : Int =0,
    val blogId : Int =0,
    val createdAt : String ="",
    val image : String="",
    val title : String="",
    val url : String=""
){
  constructor(): this(0,0,"","","","")
}