package com.jet2travel.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

/**
 * Created by tufan on 30,April,2020
 */


/**
 * Class which provides a model for post
 * @constructor Sets all properties of the post
 * @property userId the unique identifier of the author of the post
 * @property id the unique identifier of the post
 * @property title the title of the post
 * @property body the content of the post
 */
/**
 * Created by tufan on 30,April,2020
 */



@Entity(tableName = "article")
data class Article (
    @PrimaryKey
    val id : Int =0,
    val createdAt : String ="",
    val content : String ="",
    val comments : Int =0,
    val likes : Int =0,


    @TypeConverters(DataRoomConverter::class) // add here
    @ColumnInfo(name = "Media")
    val media : List<Media> = emptyList() ,

    @TypeConverters(DataRoomConverter::class) // add here
    @ColumnInfo(name = "User")
    val user : List<User> =  emptyList()
)