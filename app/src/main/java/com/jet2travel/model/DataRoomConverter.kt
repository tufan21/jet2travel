package com.jet2travel.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by tufan on 30,April,2020
 */
class DataRoomConverter {
    @TypeConverter
    fun fromArticle(articleList: List<Media>?): String? {
        if (articleList == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<Media?>?>() {}.type
        return gson.toJson(articleList, type)
    }

    @TypeConverter
    fun toArticle(articleList: String?): List<Media>? {
        if (articleList == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<Media?>?>() {}.type
        return gson.fromJson<List<Media>>(articleList, type)
    }

    @TypeConverter
    fun fromUser(userList: List<User>?): String? {
        if (userList == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<User?>?>() {}.type
        return gson.toJson(userList, type)
    }

    @TypeConverter
    fun toUser(userList: String?): List<User>? {
        if (userList == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<User?>?>() {}.type
        return gson.fromJson<List<User>>(userList, type)
    }



    @TypeConverter
    fun fromString(value: String?): List<String?>? {
        val listType =
            object : TypeToken<List<String?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: List<String?>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}