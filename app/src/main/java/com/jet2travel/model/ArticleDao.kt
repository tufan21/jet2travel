package com.jet2travel.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * Created by tufan on 30,April,2020
 */


@Dao
interface ArticleDao {
    @get:Query("SELECT * FROM article")
    val all: List<Article>

    @Insert
    fun insertAll(vararg articles: Article)
}