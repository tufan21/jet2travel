package com.jet2travel.model

import androidx.room.Entity

/**
 * Created by tufan on 30,April,2020
 */

data class User (
    val id : Int=0,
    val blogId : Int=0,
    val createdAt : String="",
    val name : String="",
    val avatar : String="",
    val lastname : String="",
    val city : String ="",
    val designation : String ="",
    val about : String =""
){
  constructor(): this(0,0,"","","","","","","")
}