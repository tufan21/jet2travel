package com.jet2travel.injection.component

import com.jet2travel.injection.module.NetworkModule
import com.jet2travel.ui.article.ArticleListViewModel
import com.jet2travel.ui.article.ArticleViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by tufan on 30,April,2020
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified ArticleListViewModel.
     * @param articleListViewModel ArticleListViewModel in which to inject the dependencies
     */
    fun inject(articleListViewModel: ArticleListViewModel)
    /**
     * Injects required dependencies into the specified PostViewModel.
     * @param articleViewModel ArticleViewModel in which to inject the dependencies
     */
    fun inject(articleViewModel: ArticleViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}