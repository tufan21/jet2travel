package com.jet2travel.network

import com.jet2travel.model.Article
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
/**
 * Created by tufan on 30,April,2020
 */
interface ArticleAPI {
    /**
     * Get the list of the pots from the API
     */
    @GET("blogs")
    fun getArticle(@Query("page") page: Int,@Query("limit") limit: Int): Observable<List<Article>>
}