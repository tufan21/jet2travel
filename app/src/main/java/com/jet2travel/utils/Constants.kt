package com.jet2travel.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by tufan on 30,April,2020
 */

/** The base URL of the API */
const val BASE_URL: String = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/"

fun parseDate(timeAtMiliseconds: String?): String? {

    if (timeAtMiliseconds.equals("", ignoreCase = true)) {
        return ""
    }
    //API.log("Day Ago "+dayago);
    val result = "now"
    val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val todayDate: String = formatter.format(Date())

    val d: Date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(timeAtMiliseconds)
    val time = d.time

    val dayagolong = time * 1000

    val agoformater: String = formatter.format(time)
    var CurrentDate: Date? = null
    var CreateDate: Date? = null
    try {
        CurrentDate = formatter.parse(todayDate)
        CreateDate = formatter.parse(agoformater)
        var different: Long =
            Math.abs(CurrentDate.getTime() - CreateDate.getTime())
        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24
        val elapsedDays = different / daysInMilli
        different = different % daysInMilli
        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli
        val elapsedMinutes = different / minutesInMilli
        different = different % minutesInMilli
        val elapsedSeconds = different / secondsInMilli
        different = different % secondsInMilli
        if (elapsedDays == 0L) {
            if (elapsedHours == 0L) {
                if (elapsedMinutes == 0L) {
                    if (elapsedSeconds < 0) {
                        return "0" + " s"
                    } else {
                        if (elapsedDays > 0 && elapsedSeconds < 59) {
                            return "now"
                        }
                    }
                } else {
                    return elapsedMinutes.toString() + " min"
                }
            } else {
                return elapsedHours.toString() + " hr"
            }
        } else {
            if (elapsedDays <= 29) {
                return elapsedDays.toString() + " day"
            }
            if (elapsedDays > 29 && elapsedDays <= 58) {
                return "1 Month"
            }
            if (elapsedDays > 58 && elapsedDays <= 87) {
                return "2 Month"
            }
            if (elapsedDays > 87 && elapsedDays <= 116) {
                return "3 Month"
            }
            if (elapsedDays > 116 && elapsedDays <= 145) {
                return "4 Month"
            }
            if (elapsedDays > 145 && elapsedDays <= 174) {
                return "5 Month"
            }
            if (elapsedDays > 174 && elapsedDays <= 203) {
                return "6 Month"
            }
            if (elapsedDays > 203 && elapsedDays <= 232) {
                return "7 Month"
            }
            if (elapsedDays > 232 && elapsedDays <= 261) {
                return "8 Month"
            }
            if (elapsedDays > 261 && elapsedDays <= 290) {
                return "9 Month"
            }
            if (elapsedDays > 290 && elapsedDays <= 319) {
                return "10 Month"
            }
            if (elapsedDays > 319 && elapsedDays <= 348) {
                return "11 Month"
            }
            if (elapsedDays > 348 && elapsedDays <= 360) {
                return "12 Month"
            }
            if (elapsedDays > 360 && elapsedDays <= 720) {
                return "1 Year"
            }
            if (elapsedDays > 720) {
                val formatterYear = SimpleDateFormat("MM/dd/yyyy")
                val calendarYear: Calendar = Calendar.getInstance()
                calendarYear.setTimeInMillis(dayagolong)
                return formatterYear.format(calendarYear.getTime()).toString() + ""
            }
        }
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return result
}