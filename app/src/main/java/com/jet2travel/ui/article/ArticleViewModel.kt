package com.jet2travel.ui.article


import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jet2travel.R
import com.jet2travel.base.BaseViewModel
import com.jet2travel.model.Article
import com.jet2travel.model.Media
import com.jet2travel.model.User
import com.jet2travel.utils.parseDate
import java.text.DecimalFormat


//import com.jet2travel.model.Media
//import com.jet2travel.model.User

/**
 * Created by tufan on 30,April,2020
 */
class ArticleViewModel: BaseViewModel() {
    companion object {
        @JvmStatic @BindingAdapter("imageURL")
        fun loadImage(imageView: ImageView, imageURL: String?) {
            Glide.with(imageView.getContext())
                .setDefaultRequestOptions(
                    RequestOptions()
                        .circleCrop()
                )
                .load(imageURL)
                .placeholder(R.drawable.ic_placeholder_img_article)
                .into(
                    imageView
                )
        }


    }
    private val articleComments = MutableLiveData<Int>()
    private val articleLikes = MutableLiveData<Int>()
    private val articleContent = MutableLiveData<String>()
    private val articleCreatedAt = MutableLiveData<String>()
    private val articleMedia = MutableLiveData<List<Media>>()
    private val articleuser = MutableLiveData<List<User>>()


    fun bind(article: Article){
        articleComments.value = article.comments
        articleLikes.value = article.likes
        articleContent.value = article.content
        articleCreatedAt.value = article.createdAt
        articleMedia.value = article.media
        articleuser.value = article.user

    }

    fun getArticleComments():MutableLiveData<Int>{
        return articleComments
    }

    fun getArticleLikes():MutableLiveData<Int>{
        return articleLikes
    }

    fun getArticleContent():MutableLiveData<String>{
        return articleContent
    }
    fun getArticleCreatedAt():MutableLiveData<String>{
        return articleCreatedAt
    }
    fun getMedia():MutableLiveData<List<Media>>{
        return articleMedia
    }
    fun getUser():MutableLiveData<List<User>>{
        return articleuser
    }
    fun getFullName(): String? {
        return articleuser.value?.get(0)?.name + " "+  return articleuser.value?.get(0)?.lastname
    }

    fun getDesignation(): String? {
        return articleuser.value?.get(0)?.designation
    }
    fun getLikes():String{
        return DecimalFormat("##.#").format(articleLikes.value!!.toDouble()/1000) +" K Likes"
    }

    fun getComments():String{
        return DecimalFormat("##.#").format(articleComments.value!!.toDouble()/1000) +" K Comments"
    }
    fun getMediaTitle(): String? {
        return articleMedia.value?.get(0)?.title
    }
    fun getMediaURl(): String? {
        return  articleMedia.value?.get(0)?.url
    }
    fun getPostedTime(): String? {
        return parseDate(articleCreatedAt.value)
    }
    fun getAvatar(): String? {
        return  articleuser.value?.get(0)?.avatar
    }
    fun getMediaImage(): String? {
        return  articleMedia.value?.get(0)?.image
    }

    // important code for loading image here

}