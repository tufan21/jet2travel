package com.jet2travel.ui.article

import androidx.lifecycle.MutableLiveData
import com.jet2travel.base.BaseViewModel
import com.jet2travel.model.Media

/**
 * Created by tufan on 30,April,2020
 */
class MediaViewModel: BaseViewModel() {
    private val mediaImage = MutableLiveData<String>()
    private val mediaTitle = MutableLiveData<String>()
    private val mediaURL = MutableLiveData<String>()

    fun bind(media: Media){
        mediaImage.value = media.image
        mediaTitle.value = media.title
        mediaURL.value = media.url
    }

    fun getMediaImage():MutableLiveData<String>{
        return mediaImage
    }
    fun getMediaTitle():MutableLiveData<String>{
        return mediaTitle
    }
    fun getMediaURL():MutableLiveData<String>{
        return mediaURL
    }
}