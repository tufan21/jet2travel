package com.jet2travel.ui.article

import androidx.lifecycle.MutableLiveData
import com.jet2travel.base.BaseViewModel
import com.jet2travel.model.User

/**
 * Created by tufan on 30,April,2020
 */
class UserViewModel: BaseViewModel() {
    private val userName = MutableLiveData<String>()
    private val userLastName = MutableLiveData<String>()
    private val userAvatar = MutableLiveData<String>()
    private val userAbout = MutableLiveData<String>()
    private val userCity = MutableLiveData<String>()
    private val userDesignation = MutableLiveData<String>()

    fun bind(user: User){
        userName.value = user.name
        userLastName.value = user.lastname
        userAvatar.value = user.avatar
        userAbout.value = user.about
        userCity.value = user.city
        userDesignation.value = user.designation
    }

    fun getUserName():MutableLiveData<String>{
        return userName
    }
    fun getUserLastName():MutableLiveData<String>{
        return userLastName
    }
    fun getUserAvatar():MutableLiveData<String>{
        return userAvatar
    }
    fun getUserAbout():MutableLiveData<String>{
        return userAbout
    }
    fun getUserCity():MutableLiveData<String>{
        return userCity
    }
    fun getUserDesignation():MutableLiveData<String>{
        return userDesignation
    }
}