package com.jet2travel.ui.article

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.jet2travel.R
import com.jet2travel.model.Article

import com.jet2travel.databinding.ItemArticleBinding

/**
 * Created by tufan on 30,April,2020
 */

class ArticleListAdapter: RecyclerView.Adapter<ArticleListAdapter.ViewHolder>() {
    private lateinit var postList:List<Article>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleListAdapter.ViewHolder {
        val binding: ItemArticleBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_article, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleListAdapter.ViewHolder, position: Int) {
        holder.bind(postList[position])
    }

    override fun getItemCount(): Int {
        return if(::postList.isInitialized) postList.size else 0
    }

    fun updatePostList(postList:List<Article>){
        this.postList = postList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemArticleBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = ArticleViewModel()

        fun bind(article: Article){
            viewModel.bind(article)
            binding.viewModel = viewModel
        }
    }
}