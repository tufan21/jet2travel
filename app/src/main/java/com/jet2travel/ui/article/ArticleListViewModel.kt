package com.jet2travel.ui.article

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.jet2travel.R
import com.jet2travel.base.BaseViewModel
import com.jet2travel.model.Article
import com.jet2travel.model.ArticleDao
import com.jet2travel.network.ArticleAPI
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by tufan on 30,April,2020
 */
class ArticleListViewModel(private val articleDao: ArticleDao):BaseViewModel(){

    @Inject
    lateinit var articleApi: ArticleAPI
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadArticle() }

    val articleListAdapter: ArticleListAdapter = ArticleListAdapter()


    private lateinit var subscription: Disposable

    init{
        loadArticle()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadArticle(){

        subscription = Observable.fromCallable { articleDao.all }
            .concatMap {
                    dbPostList ->
                if(dbPostList.isEmpty())
                    articleApi.getArticle(1,10).concatMap {
                            apiArticleList -> articleDao.insertAll(*apiArticleList.toTypedArray())
                        Observable.just(apiArticleList)
                    }
                else
                    Observable.just(dbPostList)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveArticleListStart() }
            .doOnTerminate { onRetrieveArticleListFinish() }
            .subscribe(
                { result -> onRetrieveArticleListSuccess(result) },
                { onRetrieveArticleListError() }
            )
    }
    private fun onRetrieveArticleListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }


    private fun onRetrieveArticleListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveArticleListSuccess(articleList:List<Article>){
        articleListAdapter.updatePostList(articleList)
    }

    private fun onRetrieveArticleListError(){
        errorMessage.value = R.string.post_error
    }
}
